import "./App.css";
import { useState, useEffect } from "react";
import { Container, Heading, Spinner, Text } from "@chakra-ui/react";
import HeaderBar from "./components/HeaderBar";
import InputBox from "./components/InputBox";
import CountriesContainer from "./components/CountriesContainer";

function App() {
  const [countriesData, setCountriesData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [inputValue, setInputValue] = useState("");
  const [regionValue, setRegionValue] = useState("");
  const [subRegionValue, setSubRegionValue] = useState("");
  const [sortValue, setSortValue] = useState("");
  const [landlocked, setLandlocked] = useState(false);

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((res) => res.json())
      .then((data) => {
        setLoading(false);
        setCountriesData(data);
        setError(null);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.message);
      });
  }, []);

  const handleChange = (value) => {
    setInputValue(value);
  };

  const handleRegionSelect = (value) => {
    setRegionValue(value);
  };

  const handleSubRegionSelect = (value) => {
    setSubRegionValue(value);
  };

  const handleSortChange = (value) => {
    setSortValue(value);
  };

  const handleChecked = (isLandlocked) => {
    setLandlocked(isLandlocked);
  };

  let filteredCountriesData = [
    ...countriesData
      .filter((country) => {
        return (
          country.name.common
            .toLowerCase()
            .includes(inputValue.toLowerCase()) &&
          (regionValue === "" || !country.region
            ? true
            : country.region
                .toLowerCase()
                .includes(regionValue.toLowerCase())) &&
          (subRegionValue === "" || !country.subregion
            ? true
            : country.subregion
                .toLowerCase()
                .includes(subRegionValue.toLowerCase())) &&
          country.landlocked === landlocked
        );
      })
      .sort((a, b) => {
        if (sortValue === "") {
          return 1;
        }

        const sortKey = sortValue === "population" ? "population" : "area";
        return b[sortKey] - a[sortKey];
      }),
  ];

  let regions = [
    ...filteredCountriesData.reduce((acc, country) => {
      acc.add(country.region);
      return acc;
    }, new Set()),
  ];

  let subRegions = [
    ...filteredCountriesData.reduce((acc, country) => {
      acc.add(country.subregion);
      return acc;
    }, new Set()),
  ].filter((country) => country);

  return (
    <div className="App">
      <HeaderBar />
      <Container maxW="container.xl">
        <InputBox
          handleChange={handleChange}
          handleRegionSelect={handleRegionSelect}
          handleSubRegionSelect={handleSubRegionSelect}
          regions={regions}
          subRegions={subRegions}
          inputValue={inputValue}
          regionValue={regionValue}
          subRegionValue={subRegionValue}
          handleSortChange={handleSortChange}
          handleChecked={handleChecked}
        />
        {loading ? (
          <Heading size="xl" textAlign="center">
            <Spinner
              thickness="5px"
              speed="2s"
              emptyColor="gray.200"
              color="blue.500"
              size="xl"
            />
            <Text>Loading Data...</Text>
          </Heading>
        ) : error || filteredCountriesData.length === 0 ? (
          <Heading size="xl" textAlign="center">
            No Country Found...
          </Heading>
        ) : (
          <CountriesContainer filteredCountriesData={filteredCountriesData} />
        )}
      </Container>
    </div>
  );
}

export default App;
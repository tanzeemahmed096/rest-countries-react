import {
  Card,
  CardBody,
  Image,
  Stack,
  Heading,
  Text,
  Flex,
} from "@chakra-ui/react";

const CountryCard = ({ countryData }) => {
  return (
    <Card
      direction="column"
      maxW={{ base: "sm", md: "xs" }}
      boxShadow="0 0 5px rgba(0, 0, 0, 0.3)"
      cursor="pointer"
      minH="350px"
    >
      <Image
        objectFit="cover"
        w="280px"
        h="187px"
        src={countryData.flags.svg}
        alt="Caffe Latte"
        borderTopRadius="inherit"
      />

      <Stack>
        <CardBody>
          <Heading size="md" py="0.5rem" ps="0.3rem">
            {countryData.name.common}
          </Heading>

          <Flex
            direction="column"
            py="1rem"
            ps="0.3rem"
            fontSize="1rem"
            gap="0.3rem"
          >
            <Text>
              Population:{" "}
              <span style={{ fontWeight: "400" }}>
                {countryData.population
                  ? countryData.population.toLocaleString("en-US")
                  : "No population data found."}
              </span>
            </Text>
            <Text>
              Region:{" "}
              <span style={{ fontWeight: "400" }}>
                {countryData.region ? countryData.region : "No region found."}
              </span>
            </Text>
            <Text>
              Capital:{" "}
              <span style={{ fontWeight: "400" }}>
                {countryData.capital && countryData.capital.length > 0
                  ? countryData.capital[0]
                  : "No capital found."}
              </span>
            </Text>
          </Flex>
        </CardBody>
      </Stack>
    </Card>
  );
};

export default CountryCard;

import CountryCard from "./CountryCard";
import { Flex } from "@chakra-ui/react";

const CountriesContainer = ({ filteredCountriesData }) => {
  return (
    <Flex
      direction={{ base: "column", md: "row" }}
      alignItems={{ base: "center" }}
      justifyContent={{ md: "center" }}
      flexWrap={{ base: "nowrap", md: "wrap" }}
      gap="2rem"
      p="1.5rem"
      mb="5rem"
    >
      {filteredCountriesData.map((country) => {
        return <CountryCard key={country.name.common} countryData={country} />;
      })}
    </Flex>
  );
};

export default CountriesContainer;

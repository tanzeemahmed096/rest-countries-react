import { Select, useColorMode } from "@chakra-ui/react";

const SelectDropdown = ({ handleSelect, title, options, selectValue }) => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Select
      variant="none"
      placeholder={title}
      border="none"
      outline="none"
      fontSize="0.9rem"
      size="lg"
      value={selectValue}
      boxShadow={
        colorMode === "light"
          ? "0 0 5px rgba(0, 0, 0, 0.2)"
          : "0 0 5px rgba(255, 255, 255, 0.2)"
      }
      cursor="pointer"
      _focus={{ boxShadow: "0 0 15px rgba(0, 0, 0, 0.2)" }}
      onChange={(e) => handleSelect(e.target.value)}
    >
      {options.map((option, idx) => {
        return (
          <option key={idx} value={option}>
            {option}
          </option>
        );
      })}
    </Select>
  );
};

export default SelectDropdown;

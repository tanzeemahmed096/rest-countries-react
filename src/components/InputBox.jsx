import {
  Box,
  Flex,
  FormControl,
  Input,
  InputGroup,
  InputLeftElement,
  useColorMode,
  Select,
  Checkbox,
} from "@chakra-ui/react";
import SelectDropdown from "./SelectDropdown";

const InputBox = ({
  handleChange,
  handleRegionSelect,
  handleSubRegionSelect,
  regions,
  subRegions,
  regionValue,
  subRegionValue,
  inputValue,
  handleSortChange,
  handleChecked,
}) => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Box w="100%" p="1.5rem" my="2rem">
      <FormControl>
        <Flex
          direction={{ base: "column", md: "row" }}
          gap={{ base: "2rem" }}
          justifyContent={{ md: "space-between" }}
        >
          <InputGroup
            boxShadow={
              colorMode === "light"
                ? "0 0 5px rgba(0, 0, 0, 0.2)"
                : "0 0 15px rgba(255, 255, 255, 0.2)"
            }
            maxW={{ md: "50%" }}
            size="lg"
            borderRadius="5"
          >
            <InputLeftElement
              fontSize="1.2em"
              pointerEvents="none"
              px="1rem"
              color="grey"
              children={<i className="bi bi-search search-icon"></i>}
            />
            <Input
              placeholder="Search for a country..."
              border="none"
              outline="none"
              value={inputValue}
              onChange={(e) => handleChange(e.target.value)}
              _focus={{ boxShadow: "0 0 15px rgba(0, 0, 0, 0.2)" }}
            />
          </InputGroup>

          <Flex gap="3rem" direction={{ base: "column", md: "row" }}>
            <SelectDropdown
              handleSelect={handleRegionSelect}
              title="Filter country by Region"
              options={regions}
              selectValue={regionValue}
            />
            <SelectDropdown
              handleSelect={handleSubRegionSelect}
              title="Filter country by Sub-Region"
              options={subRegions}
              selectValue={subRegionValue}
            />
          </Flex>
        </Flex>

        <Flex
          direction={{ base: "column", md: "row" }}
          gap={{ base: "2rem", md: "5rem" }}
          mt="2rem"
        >
          <Select
            placeholder="Sort Country By"
            onChange={(e) => handleSortChange(e.target.value)}
            maxW={{ md: "50%" }}
            variant="none"
            border="none"
            outline="none"
            fontSize="0.9rem"
            size="lg"
            boxShadow={
              colorMode === "light"
                ? "0 0 5px rgba(0, 0, 0, 0.2)"
                : "0 0 5px rgba(255, 255, 255, 0.2)"
            }
            cursor="pointer"
            _focus={{ boxShadow: "0 0 15px rgba(0, 0, 0, 0.2)" }}
          >
            <option value="population">Population</option>
            <option value="totalarea">Total Area</option>
          </Select>
          <Checkbox onChange={(e) => handleChecked(e.target.checked)}>
            Landlocked
          </Checkbox>
        </Flex>
      </FormControl>
    </Box>
  );
};

export default InputBox;

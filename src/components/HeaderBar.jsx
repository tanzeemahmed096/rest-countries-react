import {
  Box,
  Flex,
  Heading,
  Text,
  Spacer,
  Button,
  useColorMode,
} from "@chakra-ui/react";

const HeaderBar = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Flex
      w="100%"
      h="15vh"
      p="1.3rem"
      px={{ md: "4rem" }}
      alignItems="center"
      boxShadow="0 0 10px rgba(0, 0, 0, 0.2)"
    >
      <Box>
        <Heading size="lg">Where in the world?</Heading>
      </Box>
      <Spacer />
      <Button cursor="pointer" onClick={toggleColorMode}>
        <Flex gap="0.6rem">
          {colorMode === "light" ? (
            <i className="bi bi-circle-half"></i>
          ) : (
            <i className="bi bi-brightness-high"></i>
          )}
          <Text fontWeight="600">
            {colorMode === "light" ? "Dark" : "Light"} Mode
          </Text>
        </Flex>
      </Button>
    </Flex>
  );
};

export default HeaderBar;
